//
//  WindowController.swift
//  MainServer
//
//  Created by Arthit Thongpan on 2/19/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa

class WindowController: NSWindowController {
    override func windowDidLoad() {
        super.windowDidLoad()
    }
}

extension WindowController: NSWindowDelegate {
    func windowWillClose(_ notification: Notification) {
        NSApplication.shared.terminate(self)
    }
}
