//
//  LogServerDelegate.swift
//  MainServer
//
//  Created by Arthit Thongpan on 2/19/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

protocol ServerDelegate: class {
    func onError(msg: String)
    func receiveInfo(msg: String)
    func receiveData(msg: String)
}
