//
//  Server.swift
//  MainServer
//
//  Created by Arthit Thongpan on 2/18/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa
import CocoaAsyncSocket

// Notification
let kNotificationServerStatus = "kNotificationServerStatus"
let kNotificationClientCount  = "kNotificationClientCount"

class Server: NSObject {
    var listenSocket: GCDAsyncSocket!
    var socketQueue: DispatchQueue!
    private(set) var isRunning: Bool!
    
    fileprivate(set) var clients: [Client]! = []
    
    // Packet id
    let WELCOME_MSG = 0
    let ECHO_MSG = 1
    let WARNING_MSG = 2
    
    let READ_TIMEOUT = 15.0
    let READ_TIMEOUT_EXTENSION = 10.0
    
    weak var delegate: ServerDelegate?
    
    override init() {
        super.init()
        initialSetup()
    }
    
    private func initialSetup() {
        socketQueue = DispatchQueue(label: "socketQueue", attributes: .concurrent)
        listenSocket = GCDAsyncSocket(delegate: self, delegateQueue: socketQueue)
        
        isRunning = false
    }
    
    func startStop(port: UInt16 = 8080) {
        if !isRunning {
            var port = port
            if port < 0 || port > 65535 {
                port = 0
            }
            
            do {
                try listenSocket.accept(onPort: port)
            } catch {
                onError(msg:"Error starting server: \(error.localizedDescription)")
                return
            }
            
            receiveInfo(msg: "Server started on port \(listenSocket.localPort)")
            isRunning = true
            
        } else {
            // Stop accepting connection
            listenSocket.disconnect()
            closeAllConnection()
            receiveInfo(msg: "Stopped server")
            isRunning = false
        }
        
        postNotification(name: kNotificationServerStatus, data: ["status": isRunning])
    }
    
    func postNotification(name:String, data:  [AnyHashable : Any]? = nil) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: name), object: nil, userInfo: data)
    }
    
    func closeAllConnection() {
        socketQueue.sync {
            clients.forEach({ (client) in
                client.socket.disconnect()
            })
        }
        
        postNotification(name: kNotificationClientCount, data: ["count": clients.count])
    }
    
//    func sendData(data: Data) {
//        broadcast(data: data)
//    }
    
    func broadcast(data: Data) {
        for (_, client) in clients.enumerated() {
            client.socket.write(data, withTimeout: -1, tag: ECHO_MSG)
        }
    }
}

// MARK: GCDAsyncSocketDelegate

extension Server: GCDAsyncSocketDelegate {
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        if sock != listenSocket {
        
            DispatchQueue.main.async {
                self.receiveInfo(msg: "Client Disconnected")
            }

            socketQueue.sync {
                for (index, client) in clients.enumerated() {
                    if client.socket == sock {
                        clients.remove(at: index)
                        postNotification(name: kNotificationClientCount, data: ["count": clients.count])
                        break
                    }
                }
            }
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didAcceptNewSocket newSocket: GCDAsyncSocket) {
        socketQueue.sync {
            let client = Client(socket: newSocket, server: self)
            clients.append(client)
            postNotification(name: kNotificationClientCount, data: ["count": clients.count])
        }
        
        let port = newSocket.connectedPort
        
        DispatchQueue.main.async {
            self.receiveInfo(msg: "Accepted client on port \(port)")
        }
        
        let welcomeMsg = "Welcome to the AsyncSocket Mac Server\r\n"
        let welcomeData = welcomeMsg.data(using: .utf8)
        newSocket.write(welcomeData!, withTimeout: -1, tag: WELCOME_MSG)
        newSocket.readData(to: GCDAsyncSocket.crlfData(), withTimeout: READ_TIMEOUT, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        if tag == ECHO_MSG {
            sock.readData(to: GCDAsyncSocket.crlfData(), withTimeout: READ_TIMEOUT, tag: 0)
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        DispatchQueue.main.async {
            let strData = data.subdata(in: 0..<(data.count - 2))
            if let msg = String(data: strData, encoding: .utf8) {
                self.receiveData(msg: msg)
            } else {
                self.onError(msg: "Error converting received data into UTF-8 String")
            }
        }
        
        sock.write(data, withTimeout: -1, tag: ECHO_MSG)
    }
    
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutReadWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        if elapsed <= READ_TIMEOUT {
            let warningMsg = "Are you still there?\r\n"
            let warningData = warningMsg.data(using: .utf8)
            sock.write(warningData!, withTimeout: -1, tag: WARNING_MSG)
            return READ_TIMEOUT_EXTENSION
        }
        
        return 0.0
    }
}

// MARK: - ServerDelegate

extension Server: ServerDelegate {
    func onError(msg: String) {
        print("logError: \(msg)")
        delegate?.onError(msg: msg)
    }
    
    func receiveInfo(msg: String) {
        print("LogInfo: \(msg)")
        delegate?.receiveInfo(msg: msg)
    }
    
    func receiveData(msg: String) {
        print("LogMessage: \(msg)")
        delegate?.receiveData(msg: msg)
    }
}


