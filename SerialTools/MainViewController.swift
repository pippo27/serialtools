//
//  MainViewController.swift
//  SerialTools
//
//  Created by Arthit Thongpan on 3/11/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa
import ORSSerial

class MainViewController: NSViewController {

    @IBOutlet weak var containerViewOfNetworkViewController: NSView!
    @IBOutlet weak var containerViewOfSerialViewController: NSView!
    
    var networkViewController: NetworkViewController?
    var serialPortViewController: SerialPortViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        guard let identifier = segue.identifier else { return }
        switch identifier.rawValue {
        case "NetworkViewController":
            networkViewController =  segue.destinationController as? NetworkViewController
            networkViewController?.delegate = self
        case "SerialPortViewController":
            serialPortViewController = segue.destinationController as? SerialPortViewController
            serialPortViewController?.delegate = self
        default:
            break
        }
    }
}

extension MainViewController: NetworkViewControllerDelegate {
    func networkViewController(networkViewController: NetworkViewController, sendDataToSerialPort data: Data) {
        serialPortViewController?.sendData(data: data)
    }
}

extension MainViewController: SerialPortViewControllerDelegate {
    func serialPort(serialPort: ORSSerialPort, didReceive data: Data) {
        networkViewController?.broadcast(data: data)
    }
}
