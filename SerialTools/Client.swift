//
//  Client.swift
//  MainServer
//
//  Created by Arthit Thongpan on 2/19/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa
import CocoaAsyncSocket

class Client: NSObject {
    private(set) var socket: GCDAsyncSocket!
    private var server: Server?
    
    init(socket: GCDAsyncSocket, server: Server? =  nil) {
        super.init()
        self.socket = socket
        self.server = server
    }
}
