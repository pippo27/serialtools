//
//  AppDelegate.swift
//  SerialTools
//
//  Created by Arthit Thongpan on 3/11/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

