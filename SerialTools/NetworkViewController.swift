//
//  NetworkViewController.swift
//  MainServer
//
//  Created by Arthit Thongpan on 2/16/17.
//  Copyright © 2017 Arthit Thongpan. All rights reserved.
//

import Cocoa

protocol NetworkViewControllerDelegate: class {
    func networkViewController(networkViewController: NetworkViewController, sendDataToSerialPort data: Data)
}

class NetworkViewController: NSViewController {
    
    @IBOutlet weak var portTextField: NSTextField!
    @IBOutlet weak var startStopButton: NSButton!
    @IBOutlet var logTextView: NSTextView!
    @IBOutlet weak var connectionLabel: NSTextField!
    @IBOutlet weak var sendTextField: NSTextField!
    @IBOutlet weak var lineEndingPopUpButton: NSPopUpButton!
    
    var server: Server?
    weak var delegate: NetworkViewControllerDelegate?
    
    var shouldAddLineEnding = false
    var lineEndingString: String {
        let map = [0: "\r", 1: "\n", 2: "\r\n"]
        if let result = map[self.lineEndingPopUpButton.selectedTag()] {
            return result
        } else {
            return "\n"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupServer()
        setupNetworkNotification()
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupServer() {
        server = Server()
        server!.delegate = self
    }
    
    func broadcast(data: Data) {
        server!.broadcast(data: data)
    }
    
    func sendDataToSerialPort(string: String) {
        if let data = string.data(using: String.Encoding.utf8) {
            delegate?.networkViewController(networkViewController: self, sendDataToSerialPort: data)
        }
    }
    
    // MARK: - Actions
    
    @IBAction func startStopButtonClicked(_ sender: NSButton) {
        var port = portTextField.intValue
        if port < 0 || port > 65535 {
            port = 0
        }
        
        server!.startStop(port: UInt16(port))
    }
    
    @IBAction func send(_: AnyObject) {
        var string = self.sendTextField.stringValue
        if self.shouldAddLineEnding && !string.hasSuffix("\n") {
            string += self.lineEndingString
        }
        
        clearInputText()
        
        // Send Data
        if let data = string.data(using: String.Encoding.utf8) {
            server!.broadcast(data: data)
            updateTextView(msg: string, color: NSColor.blue)
        }
    }

    func clearInputText() {
        sendTextField.stringValue = ""
    }
    
    func setupNetworkNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(NetworkViewController.networkStatusNotification(notification:)),
                                               name: NSNotification.Name(kNotificationServerStatus),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(NetworkViewController.numberOfConnectionChanged(notification:)),
                                               name: NSNotification.Name(kNotificationClientCount),
                                               object: nil)
    }
    
    func scrollToBottom() {
        let scrollView = logTextView.enclosingScrollView
        var newScrollOrigin = NSPoint(x: 0.0, y: 0.0)

        if scrollView!.documentView!.isFlipped {
            let maxY = NSMaxY(scrollView!.documentView!.frame)
            newScrollOrigin = NSPoint(x: 0.0, y: maxY)
        }
        
        scrollView?.documentView?.scroll(newScrollOrigin)
    }
    
    func updateTextView(msg: String, color: NSColor) {
        let paragraph = "\(msg)\n"
        let attributes = [NSAttributedStringKey.foregroundColor:color]
        let attributedString = NSAttributedString(string: paragraph, attributes: attributes)
        
        logTextView.textStorage?.append(attributedString)
        scrollToBottom()
    }
    
    func updateUIState(isRunning: Bool) {
        portTextField.isEnabled = !isRunning
        startStopButton.title = (isRunning == true) ? "Stop": "Start"
    }
    
    // MARK: - Notification
    
    @objc func numberOfConnectionChanged(notification: Notification) {
        let connectionCount = notification.userInfo?["count"] as! Int
        connectionLabel.stringValue = "Connections (\(connectionCount))"
    }
    
    @objc func networkStatusNotification(notification: Notification) {
        let isRunning = notification.userInfo?["status"] as! Bool
        updateUIState(isRunning: isRunning)
    }
}

// MARK: - ServerDelegate

extension NetworkViewController: ServerDelegate {
    
    func onError(msg: String) {
        updateTextView(msg: msg, color: NSColor.red)
    }
    
    func receiveInfo(msg: String) {
        updateTextView(msg: msg, color: NSColor.purple)
    }
    
    func receiveData(msg: String) {
        sendDataToSerialPort(string: msg)
        updateTextView(msg: msg, color: NSColor.blue)
    }
}


